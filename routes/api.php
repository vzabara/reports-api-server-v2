<?php

use App\Http\Controllers\Api\ClientsController;
use App\Http\Controllers\Api\CustomersController;
use App\Http\Controllers\Api\DomainsController;
use App\Http\Controllers\Api\OrdersController;
use App\Http\Controllers\Api\OrdersPaymentsController;
use App\Http\Controllers\Api\OrdersPointsController;
use App\Http\Controllers\Api\OrdersProductsController;
use App\Http\Controllers\Api\ProductsModifiersController;
use App\Http\Controllers\Api\ReportersController;
use App\Http\Controllers\Api\SourcesController;
use App\Http\Controllers\Api\UsersController;
use Illuminate\Support\Facades\Route;

// Allowed to registered users (OAuth2 authorization)
Route::prefix('v1')->middleware('auth:api')->name('api.')->group(function() {
    Route::apiResources([
        'users' => UsersController::class,
        'clients' => ClientsController::class,
        'customers' => CustomersController::class,
        'orders' => OrdersController::class,
        'order-payments' => OrdersPaymentsController::class,
        'order-points' => OrdersPointsController::class,
        'order-products' => OrdersProductsController::class,
        'product-modifiers' => ProductsModifiersController::class,
        'sources' => SourcesController::class,
    ]);
    // full info
    Route::get('orders/find/{order_code}', [OrdersController::class, 'find'])->name('orders.find');
});

// Allowed to registered clients (api_token authorization)
Route::prefix('v1')->middleware('auth:clients')->name('api.')->group(function() {
    Route::post('customers/find', [CustomersController::class, 'find'])->name('customers.find');
    Route::get('client/{client}', [ClientsController::class, 'show'])->name('client.details'); // special route
    Route::post('orders/datatable', [OrdersController::class, 'dataTable'])->name('orders.datatable');
    Route::post('orders/import', [OrdersController::class, 'import'])->name('orders.import');
});

// VirtualMin automation (api_token authorization)
Route::prefix('v1')->middleware('auth:virtualmin')->name('api.')->group(function() {
    Route::apiResource('clients', ClientsController::class);
    Route::prefix('domains')->name('domains.')->group(function() {
        Route::get('/', [DomainsController::class, 'index'])->name('index');
        // special routes where domain parameter is sent via request variable
        Route::post('/show', [DomainsController::class, 'show'])->name('show');
        Route::post('/create', [DomainsController::class, 'store'])->name('store');
        Route::post('/update', [DomainsController::class, 'update'])->name('update');
        Route::post('/delete', [DomainsController::class, 'destroy'])->name('destroy');
    });
    Route::post('reporters', [ReportersController::class, 'store'])->name('reporters.store');
    Route::post('reporters/delete', [ReportersController::class, 'deleteByEmail'])->name('reporters.delete');
});
