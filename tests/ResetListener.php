<?php

namespace Tests;

use Mockery\Adapter\Phpunit\TestListener;
use PHPUnit\Framework\TestSuite;

class ResetListener extends TestListener
{
    static $wasCalled = false;

    public function startTestSuite(TestSuite $suite): void
    {
        if (!self::$wasCalled) {
            exec('./artisan migrate:reset');
            exec('./artisan migrate --seed');
            exec('./artisan passport:install');
            exec('./artisan cache:clear');
            exec('./artisan view:clear');
            exec('./artisan auth:token 1 >phpunit.token');
            self::$wasCalled = true;
        }
    }
}
