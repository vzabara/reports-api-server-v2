<?php

namespace Tests\Api;

class OrderProductsTest extends ApiTestCase
{
    public function testIndex()
    {
        $response = $this->getJson(route('api.order-products.index'));
        $response
            ->assertStatus(200)
            ->assertJson([
                'data'  => [],
                'links' => [],
                'meta'  => [],
            ]);
    }

    public function testCreate()
    {
        $response = $this->postJson(route('api.order-products.store'), [
            'title' => 'phpunit',
            'sku' => 'phpunit',
            'order_id' => 1,
            'quantity' => 1,
            'price' => 100.001,
            'sale_price' => 100.001,
            'total' => 100.111,
        ]);
        $response
            ->assertStatus(201)
            ->assertJson([
                'data' => [
                    'title' => 'phpunit',
                    'sku' => 'phpunit',
                    'order_id' => 1,
                    'quantity' => 1,
                    'price' => 100.001,
                    'sale_price' => 100.001,
                    'total' => 100.111,
                ],
            ]);
        $res = json_decode($response->getContent());

        return $res->data->id;
    }

    /**
     * @depends testCreate
     *
     * @param int $id
     *
     * @return mixed
     */
    public function testUpdate(int $id)
    {
        $response = $this->patchJson(route('api.order-products.update', ['order_product' => $id]), [
            'title' => 'phpunit',
            'sku' => 'phpunit',
            'order_id' => 1,
            'quantity' => 1,
            'price' => 100.111,
            'sale_price' => 100.111,
            'total' => 100.222,
        ]);
        $response
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'title' => 'phpunit',
                    'sku' => 'phpunit',
                    'order_id' => 1,
                    'quantity' => 1,
                    'price' => 100.111,
                    'sale_price' => 100.111,
                    'total' => 100.222,
                ],
            ]);

        return $id;
    }

    /**
     * @depends testUpdate
     *
     * @param int $id
     *
     * @return mixed
     */
    public function testShow(int $id)
    {
        $response = $this->getJson(route('api.order-products.show', ['order_product' => $id]));
        $response
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'title' => 'phpunit',
                    'sku' => 'phpunit',
                    'order_id' => 1,
                    'quantity' => 1,
                    'price' => 100.111,
                    'sale_price' => 100.111,
                    'total' => 100.222,
                ],
            ]);

        return $id;
    }

    /**
     * @depends testShow
     *
     * @param int $id
     *
     * @return mixed
     */
    public function testDelete(int $id)
    {
        $response = $this->deleteJson(route('api.order-products.destroy', ['order_product' => $id]));
        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);
    }
}
