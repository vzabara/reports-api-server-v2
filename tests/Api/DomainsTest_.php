<?php

namespace Tests\Api;

class DomainsTest_ extends ApiTestCase
{
    public function testIndex()
    {
        $response = $this->getJson(route('api.domains.index'));
        $response
            ->assertStatus(200)
            ->assertJson([
                'data'  => [],
                'links' => [],
                'meta'  => [],
            ]);
    }

    public function testCreate()
    {
        $response = $this->postJson(route('api.domains.store'), [
            'domain' => 'phpunit.rompos.com',
            'path' => '/home/phpunit',
            'port' => 6002,
        ]);
        $response
            ->assertStatus(201)
            ->assertJson([
                'data' => [
                    'domain' => 'phpunit.rompos.com',
                    'path' => '/home/phpunit',
                    'port' => 6002,
                ],
            ]);
        $res = json_decode($response->getContent());

        return $res->data->domain;
    }

    /**
     * @depends testCreate
     *
     * @param string $domain
     *
     * @return mixed
     */
    public function testUpdate(string $domain)
    {
        $response = $this->postJson(route('api.domains.update'), [
            'domain' => $domain,
            'port' => 6003,
        ]);
        $response
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'domain' => 'phpunit.rompos.com',
                    'path' => '/home/phpunit',
                    'port' => 6003,
                ],
            ]);

        return $domain;
    }

    /**
     * @depends testUpdate
     *
     * @param string $domain
     *
     * @return mixed
     */
    public function testShow(string $domain)
    {
        $response = $this->postJson(route('api.domains.show'), [
            'domain' => $domain,
        ]);
        $response
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'domain' => 'phpunit.rompos.com',
                    'path' => '/home/phpunit',
                    'port' => 6003,
                ],
            ]);

        return $domain;
    }

    /**
     * @depends testShow
     *
     * @param string $domain
     *
     * @return mixed
     */
    public function testDelete(string $domain)
    {
        $response = $this->postJson(route('api.domains.destroy'), [
            'domain' => $domain,
        ]);
        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);
    }
}
