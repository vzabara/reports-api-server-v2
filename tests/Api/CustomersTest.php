<?php

namespace Tests\Api;

class CustomersTest extends ApiTestCase
{
    public function testIndex()
    {
        $response = $this->getJson(route('api.customers.index'));
        $response
            ->assertStatus(200)
            ->assertJson([
                'data' => [],
                'links' => [],
                'meta' => [],
            ]);
    }

    public function testCreate()
    {
        $response = $this->postJson(route('api.customers.store'), [
            'name' => 'name',
            'phone' => '1234567890',
            'email' => 'phpunit@example.com',
            'created' => date('Y-m-d H:i:s'),
        ]);
        $response
            ->assertStatus(201)
            ->assertJson([
                'data' => [
                    'name' => 'name',
                    'phone' => '1234567890',
                    'email' => 'phpunit@example.com',
                ],
            ]);
        $res = json_decode($response->getContent());

        return $res->data->id;
    }

    /**
     * @depends testCreate
     *
     * @param int $id
     *
     * @return mixed
     */
    public function testUpdate(int $id)
    {
        $response = $this->patchJson(route('api.customers.update', ['customer' => $id]), [
            'name' => 'phpunit',
            'email' => 'phpunit@example.com',
            'created' => date('Y-m-d H:i:s'),
        ]);
        $response
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'name' => 'phpunit',
                    'email' => 'phpunit@example.com',
                ],
            ]);

        return $id;
    }

    /**
     * @depends testUpdate
     *
     * @param int $id
     *
     * @return mixed
     */
    public function testShow(int $id)
    {
        $response = $this->getJson(route('api.customers.show', ['customer' => $id]));
        $response
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'name' => 'phpunit',
                    'email' => 'phpunit@example.com',
                ],
            ]);

        return $id;
    }

    /**
     * @depends testShow
     *
     * @param int $id
     *
     * @return mixed
     */
    public function testDelete(int $id)
    {
        $response = $this->deleteJson(route('api.customers.destroy', ['customer' => $id]));
        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);
    }
}
