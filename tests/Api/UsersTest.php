<?php

namespace Tests\Api;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UsersTest extends ApiTestCase
{
    public function testIndex()
    {
        $response = $this->getJson(route('api.users.index'));
        $response
            ->assertStatus(200)
            ->assertJson([
                'data' => [],
                'links' => [],
                'meta' => [],
            ]);
    }

    public function testCreate()
    {
        $response = $this->postJson(route('api.users.store'), [
            'name' => 'name',
            'email' => 'phpunit@example.com',
            'password' => Hash::make('password'),
            'api_token' => Str::random(80),
        ]);
        $response
            ->assertStatus(201)
            ->assertJson([
                'data' => [
                    'name' => 'name',
                    'email' => 'phpunit@example.com',
                ],
            ]);
        $res = json_decode($response->getContent());

        return $res->data->id;
    }

    /**
     * @depends testCreate
     *
     * @param int $id
     *
     * @return mixed
     */
    public function testUpdate(int $id)
    {
        $response = $this->patchJson(route('api.users.update', ['user' => $id]), [
            'name' => 'phpunit',
            'email' => 'phpunit@example.com',
            'password' => Hash::make('password'),
            'api_token' => Str::random(80),
        ]);
        $response
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'name' => 'phpunit',
                    'email' => 'phpunit@example.com',
                ],
            ]);

        return $id;
    }

    /**
     * @depends testUpdate
     *
     * @param int $id
     *
     * @return mixed
     */
    public function testShow(int $id)
    {
        $response = $this->getJson(route('api.users.show', ['user' => $id]));
        $response
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'name' => 'phpunit',
                    'email' => 'phpunit@example.com',
                ],
            ]);

        return $id;
    }

    /**
     * @depends testShow
     *
     * @param int $id
     *
     * @return mixed
     */
    public function testDelete(int $id)
    {
        $response = $this->deleteJson(route('api.users.destroy', ['user' => $id]));
        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);
    }
}
