<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory()->createOne([
            'name' => 'Support',
            'email' => 'itsupport@intelagy.com',
            'privileged' => 1,
        ]);
        User::factory()->createOne([
            'name' => 'Test',
            'email' => 'test@rompos.com',
        ]);
        $this->call([
            ClientsSeeder::class,
            DomainsSeeder::class,
            SourcesSeeder::class,
            CustomersSeeder::class,
            OrdersSeeder::class,
            OrderProductsSeeder::class,
            OrderPointsSeeder::class,
            ProductModifiersSeeder::class,
            OrderPaymentsSeeder::class,
        ]);
    }
}
