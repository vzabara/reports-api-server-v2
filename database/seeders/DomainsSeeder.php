<?php

namespace Database\Seeders;

use App\Models\Client;
use App\Models\Domain;
use Illuminate\Database\Seeder;

class DomainsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Domain::create([
            'domain' => 'test.rompos.com',
            'path' => '/home/test',
            'port' => 6001,
        ]);
    }
}
