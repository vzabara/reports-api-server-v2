<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCommissionToClient extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->addColumn('double', 'commission')
                ->after('title')
                ->default(0)
                ->comment('percents');
            $table->addColumn('double', 'fee')
                ->after('commission')
                ->default(0)
                ->comment('fixed');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->dropColumn('commission');
            $table->dropColumn('fee');
        });
    }
}
