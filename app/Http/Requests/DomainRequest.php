<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class DomainRequest
 * @package App\Http\Requests
 *
 * @bodyParam domain string Domain. Example: example.rompos.com
 * @bodyParam path string Path to domain home directory. Example: /home/example/
 * @bodyParam port integer WebSocket port number. Example: 6001
 */
class DomainRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'domain' => 'required|max:100|regex:/^(?:[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?\.)+[a-z0-9][a-z0-9-]{0,61}[a-z]$/i',
            'path' => 'required|max:200',
            'port' => 'required|integer',
        ];
    }
}
