<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ClientRequest
 * @package App\Http\Requests
 *
 * @bodyParam user_id integer Existing OAuth client ID. Example: 1
 * @bodyParam title string Client title. Example: Restaurant
 * @bodyParam commission numeric Commission (percents). Example: 3
 * @bodyParam fee numeric Fee (fixed). Example: 2.5
 */
class ClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:200',
            'commission' => 'numeric',
            'fee' => 'numeric',
            'user_id' => 'required|exists:users,id',
        ];
    }
}
