<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class OrderPaymentRequest
 * @package App\Http\Requests
 *
 * @bodyParam type string required Order payment type. Example: creditcard
 * @bodyParam amount numeric required Payment amount. Example: 101.005
 * @bodyParam operation string required Operation type. Example: incoming
 * @bodyParam order_id numeric required Existing order ID. Example: 1
 * @bodyParam created date required Payment date. Example: 2020-02-21 15:14:05
 */
class OrderPaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required|max:100',
            'amount' => 'required|numeric',
            'operation' => 'required|max:100',
            'order_id' => 'required|exists:orders,id',
            'created' => 'required|date',
        ];
    }
}
