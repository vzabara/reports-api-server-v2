<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CustomerRequest;
use App\Http\Resources\CustomerResource;
use App\Models\Customer;
use App\Repositories\Customers\CustomersRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\CollectsResources;

/**
 * @group Customers
 *
 * Class CustomersController
 * @package App\Http\Controllers\Api
 */
class CustomersController extends Controller
{
    public function __construct()
    {
        $this->repository = new CustomersRepository();
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return CollectsResources
     *
     * @authenticated
     */
    public function index(Request $request)
    {
        return CustomerResource::collection($this->repository->filter($request)->page());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CustomerRequest $request
     *
     * @return CustomerResource
     *
     * @authenticated
     */
    public function store(CustomerRequest $request)
    {
        return new CustomerResource(Customer::create($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     *
     * @return CustomerResource
     *
     * @authenticated
     */
    public function show(Customer $customer)
    {
        return new CustomerResource($customer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CustomerRequest $request
     * @param  \App\Models\Customer $customer
     * @return CustomerResource
     * @authenticated
     */
    public function update(CustomerRequest $request, Customer $customer)
    {
        $customer->update($request->all());

        return new CustomerResource($customer);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Customer  $customer
     *
     * @return \Illuminate\Http\Response
     *
     * @authenticated
     */
    public function destroy(Customer $customer)
    {
        $customer->delete();

        return response()->json_success();
    }

    public function find(Request $request)
    {
        return response()->json([
            'user' => Customer::where('email', $request->email)->first(),
        ]);
    }
}
