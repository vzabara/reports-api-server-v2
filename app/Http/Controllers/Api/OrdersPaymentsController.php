<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\OrderPaymentRequest;
use App\Http\Resources\OrderPaymentResource;
use App\Models\OrderPayment;
use App\Repositories\OrderPayments\OrderPaymentsRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\CollectsResources;

/**
 * @group Order Payments
 *
 * Class OrdersPaymentsController
 * @package App\Http\Controllers\Api
 */
class OrdersPaymentsController extends Controller
{
    public function __construct()
    {
        $this->repository = new OrderPaymentsRepository();
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return CollectsResources
     *
     * @authenticated
     */
    public function index(Request $request)
    {
        return OrderPaymentResource::collection($this->repository->filter($request)->page());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param OrderPaymentRequest $request
     *
     * @return OrderPaymentResource
     *
     * @authenticated
     */
    public function store(OrderPaymentRequest $request)
    {
        return new OrderPaymentResource(OrderPayment::create($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OrderPayment  $orderPayment
     *
     * @return OrderPaymentResource
     *
     * @authenticated
     */
    public function show(OrderPayment $orderPayment)
    {
        return new OrderPaymentResource($orderPayment);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param OrderPaymentRequest $request
     * @param  \App\Models\OrderPayment $orderPayment
     * @return OrderPaymentResource
     * @authenticated
     */
    public function update(OrderPaymentRequest $request, OrderPayment $orderPayment)
    {
        $orderPayment->update($request->all());

        return new OrderPaymentResource($orderPayment);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OrderPayment  $orderPayment
     *
     * @return \Illuminate\Http\Response
     *
     * @authenticated
     */
    public function destroy(OrderPayment $orderPayment)
    {
        $orderPayment->delete();

        return response()->json_success();
    }
}
