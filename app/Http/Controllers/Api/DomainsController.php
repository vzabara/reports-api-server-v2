<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\DomainRequest;
use App\Http\Resources\DomainResource;
use App\Models\Domain;
use App\Repositories\Domains\DomainsRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\CollectsResources;

/**
 * @group Clients
 *
 * Class DomainsController
 * @package App\Http\Controllers\Api
 */
class DomainsController extends Controller
{
    public function __construct()
    {
        $this->repository = new DomainsRepository();
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return CollectsResources
     *
     * @authenticated
     */
    public function index(Request $request)
    {
        return DomainResource::collection($this->repository->filter($request)->page());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param DomainRequest $request
     *
     * @return DomainResource
     *
     * @authenticated
     */
    public function store(DomainRequest $request)
    {
        return new DomainResource(Domain::create($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     *
     * @return DomainResource
     *
     * @authenticated
     */
    public function show(Request $request)
    {
        $domain = Domain::where('domain', $request->domain)->first();

        return $domain ? new DomainResource($domain) : response()->json_error();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param DomainRequest $request
     * @return DomainResource
     * @authenticated
     */
    public function update(Request $request)
    {
        if ($domain = Domain::where('domain', $request->domain)->first()) {
            $domain->update($request->all());

            return new DomainResource($domain);
        }

        return response()->json_error();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Domain  $domain
     *
     * @return \Illuminate\Http\Response
     *
     * @authenticated
     */
    public function destroy(Request $request)
    {
        Domain::where('domain', $request->domain)->delete();

        return response()->json_success();
    }
}
