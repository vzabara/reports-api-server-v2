<?php

namespace App\Http\Resources;

use App\Http\Controllers\Controller;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'type' => $this->type,
            'total' => $this->total,
            'fee' => $this->fee,
            'status' => $this->status,
            'client_id' => $this->client_id,
            'created' => $this->created,
            'source_id' => $this->when(!Controller::$fullInfo, $this->source_id),
            $this->mergeWhen(Controller::$fullInfo, [
                'products' => OrderProductResource::collection($this->products()->get()),
                'payments' => OrderPaymentResource::collection($this->payments()->get()),
                'points' => OrderPointResource::collection($this->points()->get()),
                'customer' => new CustomerResource($this->customer()->first()),
                'source' => new SourceResource($this->source()->first()),
            ]),
        ];
    }
}
