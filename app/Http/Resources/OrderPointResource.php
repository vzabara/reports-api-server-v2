<?php

namespace App\Http\Resources;

use App\Http\Controllers\Controller;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderPointResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'order_id' => $this->when(!Controller::$fullInfo, $this->order_id),
            'customer_id' => $this->customer_id,
            'points_earned' => $this->points_earned,
            'points_redeemed' => $this->points_redeemed,
            'redemption' => $this->redemption,
        ];
    }
}
