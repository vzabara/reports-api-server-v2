<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = [
        'title',
        'commission',
        'fee',
        'user_id',
        'warehouse_id',
        'is_active',
        // controlled by Virtualmin
        // enable/disable ROMPOS plugin
        'is_enabled',
    ];

    protected $casts = [
        'commission' => 'double',
        'fee' => 'double',
        'is_active' => 'boolean',
        'is_enabled' => 'boolean',
    ];
}
