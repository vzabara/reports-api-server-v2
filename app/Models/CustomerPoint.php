<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerPoint extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'customer_id',
        'points',
    ];
}
