<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductModifier extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'order_product_id',
        'name',
        'price',
    ];

    protected $casts = [
        'price' => 'decimal:3',
    ];
}
