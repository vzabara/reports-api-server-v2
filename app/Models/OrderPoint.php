<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderPoint extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'order_id',
        'customer_id',
        'points_earned',
        'points_redeemed',
        'redemption',
    ];

    protected $casts = [
        'points_earned' => 'integer',
        'points_redeemed' => 'integer',
        'redemption' => 'decimal:3',
    ];
}
