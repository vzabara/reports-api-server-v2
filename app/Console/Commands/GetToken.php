<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class GetToken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth:token {parameter : User ID or user email}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get OAuth token';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $parameter = $this->argument('parameter');
        if (is_numeric($parameter)) {
            $user = User::findOrFail($parameter);
        } else {
            $user = User::where('email', $parameter)->first();
        }
        /** @var $res \Laravel\Passport\PersonalAccessTokenResult */
        $res = $user->createToken(null, ['*']);
        $token = $res->accessToken;
        $this->info($token);
    }
}
