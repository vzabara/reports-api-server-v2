<?php
/**
 * Created by PhpStorm.
 * User: Vladimir Zabara <wlady2001@gmail.com>
 * Date: 1/10/21
 * Time: 10:40 AM
 */

namespace App\Repositories\Clients\Filters;

use App\Repositories\Filter;
use Illuminate\Database\Eloquent\Builder;

class UserId extends Filter
{
    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder

     * @return Builder $builder
     */
    public function apply(Builder $builder)
    {
        return $builder->where('user_id', $this->operator, $this->value);
    }
}
