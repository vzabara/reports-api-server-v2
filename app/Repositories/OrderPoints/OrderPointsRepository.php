<?php
/**
 * Created by PhpStorm.
 * User: Vladimir Zabara <wlady2001@gmail.com>
 * Date: 10/21/19
 * Time: 3:59 PM
 */

namespace App\Repositories\OrderPoints;

use App\Models\OrderPoint;
use App\Repositories\BaseRepository;

class OrderPointsRepository extends BaseRepository
{
    protected static $model = OrderPoint::class;
}