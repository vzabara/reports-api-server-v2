<?php
/**
 * Created by PhpStorm.
 * User: Vladimir Zabara <wlady2001@gmail.com>
 * Date: 10/21/19
 * Time: 3:59 PM
 */

namespace App\Repositories\Domains;

use App\Models\Domain;
use App\Repositories\BaseRepository;

class DomainsRepository extends BaseRepository
{
    protected static $model = Domain::class;
}
