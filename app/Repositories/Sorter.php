<?php
/**
 * Created by PhpStorm.
 * User: Vladimir Zabara <wlady2001@gmail.com>
 * Date: 10/22/19
 * Time: 10:15 AM
 */

namespace App\Repositories;

use Illuminate\Database\Eloquent\Builder;

abstract class Sorter
{
    protected $direction;

    public function __construct($direction = 'ASC')
    {
        $this->direction = $direction;

        return $this;
    }

    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder

     * @return Builder $builder
     */
    abstract public function apply(Builder $builder);
}